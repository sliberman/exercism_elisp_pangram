;;; pangram.el --- Pangram (exercism)

;;; Commentary:

;;; Code:
(defun is-pangram (string)
  "Return true whether the string is or not a pangram"
  (let ((letters-used 0)
	(target (string-to-number "3FFFFFF" 16))
	(i 0)
	(n (length string))
	(string (downcase string)))
    (while (< i n)
      (let ((char (aref string i)))
	(if (and (<= char ?z) (>= char ?a))
	    (setq letters-used (logior letters-used (expt 2 (- char ?a))))))
      (setq i (1+ i)))
    (= letters-used target)))

(provide 'pangram)
;;; pangram.el ends here
